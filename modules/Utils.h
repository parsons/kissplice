#ifndef UTILS_H
#define UTILS_H

#include <string>
#define MAX 1024
#define NUMBEROFFILES 128
#define MIN(a,b) ((a) < (b) ? (a) : (b)) 
#define MIN3(a,b,c) ((a) < (b) ? ((a) < (c) ? (a) : (c)) : ((b) < (c) ? (b) : (c)))
using namespace std;

string to_str( char buf[] );
string to_str( int u );

int edit_distance(const void *s1, size_t l1, const void *s2, size_t l2, size_t nmemb, int (*comp)(const void*, const void*));
int hamming_distance(const void *s1, size_t l1, const void *s2, size_t l2, size_t nmemb, int (*comp)(const void*, const void*));
int comp(const void *a, const void *b); 

char complement(char b);
string reverse_complement(string seq);
char reverse_dir(char dir);

FILE* open_file( char* filename );
string toLowerContext( const string &sequence, const int contextFirst, const int contextLast);
#endif
