/* ***************************************************************************
 *
 *                              KisSplice
 *      de-novo calling alternative splicing events from RNA-seq data.
 *
 * ***************************************************************************
 *
 * Copyright INRIA
 *  contributors :  Vincent Lacroix
 *                  Pierre Peterlongo
 *                  Gustavo Sacomoto
 *                  Vincent Miele
 *                  Alice Julien-Laferriere
 *                  David Parsons
 *
 * pierre.peterlongo@inria.fr
 * vincent.lacroix@univ-lyon1.fr
 *
 * This software is a computer program whose purpose is to detect alternative
 * splicing events from RNA-seq data.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */


#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>
#include "Utils.h"
#include "NGraph.h"
using namespace std;

struct block {
  string l[4];  
};

bool comp_block(block a, block b) 
{
  string a_up = a.l[1];
  string a_low = a.l[3];
  string b_up = b.l[1];
  string b_low = b.l[3];
  
  // upper path is the lexicograficaly smaller, canonical form.
  if (a_low < a_up) swap(a_low, a_up);
  if (b_low < b_up) swap(b_low, b_up);

  return (a_up < b_up) || (a_up == b_up && a_low < b_low);
}

bool eq(block a, block b) 
{
  string a_up = a.l[1], a_low = a.l[3], b_up = b.l[1], b_low = b.l[3];
  
  // upper path is the lexicograficaly smaller, canonical form.
  if (a_low < a_up) swap(a_low, a_up);
  if (b_low < b_up) swap(b_low, b_up);

  return a_up == b_up && a_low == b_low;
}  

block reverse_complement(block &b)
{
  block res;
  
  res.l[1] = reverse_complement(b.l[1]);
  res.l[3] = reverse_complement(b.l[3]);
  
  res.l[0] = b.l[0];
  res.l[2] = b.l[2];

  return res;
}

int main(int argc, char **argv)
{
  ifstream bubbles_file;
  bubbles_file.open(argv[1]);
  
  string line;
  int nlines = 0;
  block cur;
  vector<block> all_blocks;

  while (bubbles_file.good())
  {
    getline(bubbles_file, line);
    nlines++;

    cur.l[(nlines-1) % 4] = line;
    if (nlines % 4 == 0)
    { 
      // convert the sequences to a canonical form, lexicografically smaller (forward and reverse) 
      if (comp_block(reverse_complement(cur), cur))
      	cur = reverse_complement(cur);
      
      all_blocks.push_back(cur);
    }
  }
  
  sort(all_blocks.begin(), all_blocks.end(), comp_block);
  vector<block>::iterator end = unique(all_blocks.begin(), all_blocks.end(), eq);
    
  vector<block>::iterator it; 
  for (it = all_blocks.begin(); it != end; it++)
    for (int j = 0; j < 4; j++)
      cout << it->l[j] << "\n";

  bubbles_file.close();
  return 0;
}
