# CMake generated Testfile for 
# Source directory: /home/vincent/KisSplice/kissplice/bcalm/gatb-core/gatb-core/thirdparty/hdf5
# Build directory: /home/vincent/KisSplice/kissplice/build/bcalm/ext/gatb-core/thirdparty/hdf5
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("src")
subdirs("tools")
