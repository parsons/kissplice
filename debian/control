Source: kissplice
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: David Parsons <david.parsons@inria.fr>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-python,
               cmake,
               python3-dev,
               libpython3-stdlib,
               zlib1g-dev,
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/med-team/kissplice
Vcs-Git: https://salsa.debian.org/med-team/kissplice.git
Homepage: http://kissplice.prabi.fr/
Rules-Requires-Root: no

Package: kissplice
Architecture: any-amd64 any-arm64 any-mips64 any-mips64el any-ia64 any-ppc64 any-ppc64el any-sparc64
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends}
Description: Detection of various kinds of polymorphisms in RNA-seq data
 KisSplice is a piece of software that enables the analysis of RNA-seq data
 with or without a reference genome. It is an exact local transcriptome
 assembler that allows one to identify SNPs, indels and alternative splicing
 events. It can deal with an arbitrary number of biological conditions, and
 will quantify each variant in each condition.
 It has been tested on Illumina datasets of up to 1G reads.
 Its memory consumption is around 5Gb for 100M reads.
