#!/usr/bin/env python3
import re
from sys import argv
from os.path import dirname, abspath
import ProcessLauncher
TEST_INSTDIR=dirname(abspath(argv[0])) 
command_line = argv[1]+"/kissplice -s 2 -k 25 -M 1000 -g "+TEST_INSTDIR+"/data/graph_HBM75brain_100000_HBM75liver_100000_k25 -C 0"
result = ProcessLauncher.run(command_line)
print(result)

# testing expected results
successful = True
if not (re.search('0a: Single SNPs, Inexact Repeats or sequencing substitution errors, 83 found', result.decode())):
    successful = False

if not (re.search('0b: Multiple SNPs, Inexact Repeats or sequencing substitution errors, 17 found', result.decode())):
    successful = False

if not (re.search('1: Alternative Splicing Events, 1 found', result.decode())):
    successful = False

if not (re.search('2: Inexact Tandem Repeats, 0 found', result.decode())):
    successful = False

if not (re.search('3: Short Indels.*, 16 found', result.decode())):
    successful = False

if not (re.search('4: All others.*, 0 found', result.decode())):
    successful = False

# summary
if successful:
    print("kisspliceGraphTest.py: test SUCCESSFUL")
else:
    print("kisspliceGraphTest.py: test FAILED")




